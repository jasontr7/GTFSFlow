resource "archive_file" "read_gtfs_zip" {
    type = "zip"
    source_dir = "${var.code_dir}"
    output_path = "${path.root}/read_gtfs.zip"
}

resource "google_storage_bucket" "cloud_functions_bucket" {
    name = "${var.project}_cloud_functions"
}

resource "google_storage_bucket_object" "read_gtfs_object" {
    name = "read_gtfs_function.zip"
    bucket = "${google_storage_bucket.cloud_functions_bucket.name}"
    source = "${archive_file.read_gtfs_zip.output_path}"
}
resource "google_cloudfunctions_function" "read_gtfs" {
    name = "read_gtfs"
    description = "Reads GTFS Realtime data"

    region = "us-central1"

    source_archive_bucket = "${google_storage_bucket.cloud_functions_bucket.name}"
    source_archive_object = "${google_storage_bucket_object.read_gtfs_object.name}"

    trigger_http = true
    available_memory_mb = 128
    runtime = "python37"
    max_instances = 1

}