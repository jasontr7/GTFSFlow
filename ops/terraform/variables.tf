variable "code_dir" {
    type = string
    default = "../../gtfs_flow"
}

variable "project" {
    type = string
    default = "gtfsflow"
}