provider "google" {
  credentials = "${file("../creds/serviceaccount.json")}"
  project     = "${var.project}"
  region      = "australia-southeast1"
  zone        = "australia-southeast1-b"
}

module "read_gtfs" {
    source   = "./read_gtfs"

    code_dir = "${var.code_dir}/read_gtfs"
    project  = "${var.project}"
}